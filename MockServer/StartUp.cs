﻿namespace MockServer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.IO;

    using Owin;

    using Nowin;

    public class StartUp
    {

        public static readonly IDictionary<string, string> MimeTypes = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase) 
        {
            { ".js"   , "application/javascript" },
            { ".html" , "text/html" },
            { ".css"  , "text/css" },
            { ".jpg"  , "image/jpeg" },
            { ".jpeg" , "image/jpeg" },
            { ".gif"  , "image/gif" },
            { ".png"  , "image/png" }
        };

        /// <summary>
        /// Gets or sets the StaticFileDirectory
        /// </summary>
        public string StaticFileDirectory { get; set; }


        public void Configure(IAppBuilder app)
        {

            app.Run(c =>
            {

                var physicalPath = StaticFileDirectory ?? Directory.GetCurrentDirectory();
                c.Environment["server.PhysicalDirectory"] = physicalPath;

                var path = c.Environment[OwinKeys.RequestPath] as string;
                if (path == "/")
                {
                    c.Response.StatusCode = 200;
                    c.Response.ContentType = "text/html";
                    return c.Response.WriteAsync(File.ReadAllBytes(Path.Combine(physicalPath, "Index.html")));
                }
                if (path.Contains(".."))
                {
                    // hackers ..
                    c.Response.StatusCode = 500;
                    return Task.Delay(0);
                }
                var p = Path.Combine(physicalPath, path.Substring(1));
                var contentType = "text/html";
                if (FileExistsAndHasMimeType(p, out contentType))
                {
                    c.Response.StatusCode = 200;
                    c.Response.ContentType = contentType;
                    return c.Response.WriteAsync(File.ReadAllBytes(p));
                }

                // otherwise we don't know what anything
                c.Environment[OwinKeys.ResponseStatusCode] = 404;
                return Task.Delay(0);

            });

        }

        private bool FileExistsAndHasMimeType(string filePath, out string contentType)
        {
            contentType = "";
            var mimeType = "";
            var ext = "";

            var extStart = filePath.LastIndexOf('.');
            if (extStart > 0)
            {
                ext = filePath.Substring(extStart);
                if (MimeTypes.TryGetValue(ext, out mimeType) &&
                    File.Exists(filePath))
                {
                    contentType = mimeType;
                    return true;
                }
            }

            return false;
        }

    }
}
