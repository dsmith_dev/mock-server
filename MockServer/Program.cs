﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Owin.Builder;

using Nowin;

namespace MockServer
{
    class Program
    {
        static void Main(string[] args)
        {

            int port = 8888;

            if (args.Length > 0)
            {
                port = int.Parse(args[0]);
            }

            var owinBuilder = new AppBuilder();
            OwinServerFactory.Initialize(owinBuilder.Properties);

            // wire up server
            var startUp = new StartUp();

            if (args.Length > 1)
            {
                startUp.StaticFileDirectory = args[1];
            }

            startUp.Configure(owinBuilder);

            var builder = ServerBuilder.New()
                .SetPort(port)
                .SetOwinApp(owinBuilder.Build())
                .SetOwinCapabilities((IDictionary<string, object>)owinBuilder.Properties[OwinKeys.ServerCapabilitiesKey]);

            using (var server = builder.Build())
            {

                Task.Run(() => server.Start());

                Console.WriteLine(string.Format("Listening on port {0}.  Enter to exit.", port));
                Console.ReadLine();
            }

        }
    }
}
